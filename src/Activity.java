/**
 *
 * @author promise7091
 * Email: promise7091@gmail.com
 * Website: http://Barjoueian.com
 *
 */
/// <summary>
/// Describes an activity according to the Critical Path Method.
/// </summary>
public class Activity {

    private String id;
    private String description;
    private int duration;
    private int est;
    private int lst;
    private int eet;
    private int let;
    private Activity[] successors;
    private Activity[] predecessors;

    public Activity() {
    }

    /// <summary>
    /// Identification concerning the activity.
    /// </summary>
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /// <summary>
    /// Brief description concerning the activity.
    /// </summary>
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /// <summary>
    /// Total amount of time taken by the activity.
    /// </summary>
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    /// <summary>
    /// Earliest start time
    /// </summary>
    public int getEst() {
        return est;
    }

    public void setEst(int est) {
        this.est = est;
    }

    /// <summary>
    ///  Latest start time
    /// </summary>
    public int getLst() {
        return lst;
    }

    public void setLst(int lst) {
        this.lst = lst;
    }

    /// <summary>
    /// Earliest end time
    /// </summary>
    public int getEet() {
        return eet;
    }

    public void setEet(int eet) {
        this.eet = eet;
    }

    /// <summary>
    /// Latest end time
    /// </summary>
    public int getLet() {
        return let;
    }

    public void setLet(int let) {
        this.let = let;
    }

    /// <summary>
    /// Activities that come after the activity.
    /// </summary>
    public Activity[] getSuccessors() {
        return successors;
    }

    public void setSuccessors(Activity[] successors) {
        this.successors = successors;
    }

    /// <summary>
    /// Activities that come before the activity.
    /// </summary>
    public Activity[] getPredecessors() {
        return predecessors;
    }

    public void setPredecessors(Activity[] predecessors) {
        this.predecessors = predecessors;
    }
    public void setPredecessorsIndexed(Activity predecessor, int index)
    {
        this.predecessors[index] = predecessor;
    }

    /// <summary>
    /// Performs a check to verify if an activity exists.
    /// </summary>
    /// <param name="list">Array storing the activities already entered.</param>
    /// <param name="id">ID being checked.</param>
    /// <param name="i">Current activities' array index.</param>
    /// <returns>Found activity or null.</returns>
    public Activity CheckActivity(Activity[] list, String id, int i) {
        for (int j = 0; j < i; j++) {
            if (list[j].id.equals(id)) {
                return list[j];
            }
        }
        return null;
    }

    /// <summary>
    /// Returns the index of a given activity.
    /// </summary>
    /// <param name="aux">Activity serving as an auxiliary referencing an existing
    /// activity.</param>
    /// <param name="i">Current activities' array index.</param>
    /// <returns>index</returns>
    public int GetIndex(Activity[] list, Activity aux, int i) {
        for (int j = 1; j < i; j++) {
            if (list[j].id.equals(aux.id)) {
                return j;
            }
        }
        return 0;
    }

    /// <summary>
    /// Fills out the aux's array of successors by checking if it has already been
    /// filled up.
    /// If so, instantiates a new aux2 that'll store the aux's current
    /// successors plus the activity that's being entered. After that, aux receives the
    /// reference of aux2.
    /// Otherwise, store the activity being entered in the first index of aux's
    /// successors array.  
    /// </summary>
    /// <param name="aux">Activity serving as an auxiliary referencing an existing
    /// activity.</param>
    /// <param name="activity">Activity being entered.</param>
    /// <returns>aux</returns>
    public Activity SetSuccessors(Activity aux, Activity activity) {
        if (aux.successors != null) {
            Activity aux2 = new Activity();
            aux2.successors = new Activity[aux.successors.length + 1];
//            aux.successors.CopyTo(aux2.successors, 0);
            
//            for(int i=0; i<aux.successors.length; i++)
//            {
//                aux2.successors[i] = aux.successors[i]; 
//            }
            System.arraycopy(aux.successors, 0, aux2.successors, 0, aux.successors.length);
            
            aux2.successors[aux.successors.length] = activity;
            aux.successors = aux2.successors;
        } else {
            aux.successors = new Activity[1];
            aux.successors[0] = activity;
        }
        return aux;
    }
}
