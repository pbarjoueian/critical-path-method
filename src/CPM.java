import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author promise7091
 * Email: promise7091@gmail.com
 * Website: http://Barjoueian.com
 *
 */
public class CPM {

    /// <summary>
    /// Implements a Critical Path Method framework.
    /// </summary>
    public CPM() {
        // TODO: Add Constructor Logic here
    }

    /// <summary>
    /// Number of activities
    /// </summary>
    private static int na;
    private static InputStreamReader sr = new InputStreamReader(System.in);
    private static BufferedReader input = new BufferedReader(sr);

    public static void main(String[] args) throws IOException {
        do {
            // Array to store the activities that'll be evaluated.
            Activity[] list = null;

            list = GetActivities(list);
            list = WalkListAhead(list);
            list = WalkListAback(list);

            CriticalPath(list);

            System.out.println("Do you wanna a new critical path solution? y\\n: ");
        } while (input.readLine().equals("y") || input.readLine().equals("Y"));
    }

    /// <summary>
    /// Gets the activities that'll be evaluated by the critical path method.
    /// </summary>
    /// <param name="list">Array to store the activities that'll be evaluated.</param>
    /// <returns>list</returns>
    private static Activity[] GetActivities(Activity[] list) throws IOException {
        do {
            System.out.println("\n       Number of activities: ");
            if ((na = Integer.parseInt(input.readLine())) < 2) {
                System.out.println("\n Invalid entry. The number must be >= 2.\n");
            }
        } while (na < 2);

        list = new Activity[na];

        for (int i = 0; i < na; i++) {
            Activity activity = new Activity();
            int index = i + 1;

            System.out.println("\n                Activity " + index + "\n");

            System.out.println("\n                     ID: " + index);
            activity.setId(input.readLine());

            System.out.println("            Description: " + index);
            activity.setDescription(input.readLine());

            System.out.println("               Duration: " + index);
            activity.setDuration(Integer.parseInt(input.readLine()));

            System.out.println(" Number of predecessors: " + index);
            int np = Integer.parseInt(input.readLine());

            if (np != 0) {
                activity.setPredecessors(new Activity[np]);

                String id;

                for (int j = 0; j < np; j++) {
                    int jIndex = j + 1;
                    System.out.println("    #" + jIndex + "predecessor's ID: ");
                    id = input.readLine();

                    Activity aux = new Activity();

                    if ((aux = aux.CheckActivity(list, id, i)) != null) {
                        activity.setPredecessorsIndexed(aux, j);

                        list[aux.GetIndex(list, aux, i)] = aux.SetSuccessors(aux, activity);
                    } else {
                        System.out.println("\n No match found! Try again.\n\n");
                        j--;
                    }
                }
            }
            list[i] = activity;
        }

        return list;
    }

    /// <summary>
    /// Performs the walk ahead inside the array of activities calculating for each
    /// activity its earliest start time and earliest end time.
    /// </summary>
    /// <param name="list">Array storing the activities already entered.</param>
    /// <returns>list</returns>
    private static Activity[] WalkListAhead(Activity[] list) {
        list[0].setEet(list[0].getEst() + list[0].getDuration());

        for (int i = 1; i < na; i++) {
            for (Activity activity : list[i].getPredecessors()) {
                if (list[i].getEst() < activity.getEet()) {
                    list[i].setEst(activity.getEet());
                }
            }

            list[i].setEet(list[i].getEst() + list[i].getDuration());
        }

        return list;
    }

    /// <summary>
    /// Performs the walk aback inside the array of activities calculating for each
    /// activity its latest start time and latest end time.
    /// </summary>
    /// <param name="list">Array storing the activities already entered.</param>
    /// <returns>list</returns>
    private static Activity[] WalkListAback(Activity[] list) {
        list[na - 1].setLet(list[na - 1].getEet());
        list[na - 1].setLst(list[na - 1].getLet() - list[na - 1].getDuration());

        for (int i = na - 2; i >= 0; i--) {
            for (Activity activity : list[i].getSuccessors()) {
                if (list[i].getLet() == 0) {
                    list[i].setLet(activity.getLst());
                } else if (list[i].getLet() > activity.getLst()) {
                    list[i].setLet(activity.getLst());
                }
            }

            list[i].setLst(list[i].getLet() - list[i].getDuration());
        }

        return list;
    }

    /// <summary>
    /// Calculates the critical path by verifyng if each activity's earliest end time
    /// minus the latest end time and earliest start time minus the latest start
    /// time are equal zero. If so, then prints out the activity id that match the
    /// criteria. Plus, prints out the project's total duration. 
    /// </summary>
    /// <param name="list">Array containg the activities already entered.</param>
    private static void CriticalPath(Activity[] list) {
        System.out.println("\n          Critical Path: ");

        for (Activity activity : list) {
            if ((activity.getEet() - activity.getLet() == 0) && (activity.getEst() - activity.getLst() == 0)) {
                System.out.println(activity.getId());
            }
        }

        System.out.println("\n\n         Total duration: " + list[list.length - 1].getEet() + "\n\n");
    }

}
